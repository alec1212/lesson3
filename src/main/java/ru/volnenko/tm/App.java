package ru.volnenko.tm;

import org.reflections.Reflections;
import ru.volnenko.tm.command.AbstractCommand;
import ru.volnenko.tm.command.ConcreateCommand.ProjectCommands.*;
import ru.volnenko.tm.command.ConcreateCommand.StorageCommands.LoadFromJsonCommand;
import ru.volnenko.tm.command.ConcreateCommand.StorageCommands.LoadFromXmlCommand;
import ru.volnenko.tm.command.ConcreateCommand.StorageCommands.SaveToJsonCommand;
import ru.volnenko.tm.command.ConcreateCommand.StorageCommands.SaveToXmlCommand;
import ru.volnenko.tm.command.ConcreateCommand.SystemCommands.ExitCommand;
import ru.volnenko.tm.command.ConcreateCommand.SystemCommands.HelpCommand;
import ru.volnenko.tm.command.ConcreateCommand.SystemCommands.VersionCommand;
import ru.volnenko.tm.command.ConcreateCommand.TaskCommands.*;
import ru.volnenko.tm.service.Bootstrap;

import java.util.Set;

/**
 * Тестовое приложение
 */
public class App {

    public static void main(final String[] args) throws Exception {

        Bootstrap bootstrap = new Bootstrap();

        Reflections reflections = new Reflections("ru.volnenko.tm");
        Set<Class<? extends AbstractCommand>> commandClasses = reflections.getSubTypesOf(AbstractCommand.class);


        for (Class cmdClass : commandClasses) {
            if (AbstractCommand.class.isAssignableFrom(cmdClass)) {
                AbstractCommand cmd = (AbstractCommand) cmdClass.newInstance();
                bootstrap.registry(cmd);
            }
        }

        bootstrap.start();
    }







}


