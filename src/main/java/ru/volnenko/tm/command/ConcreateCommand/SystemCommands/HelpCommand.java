package ru.volnenko.tm.command.ConcreateCommand.SystemCommands;

import ru.volnenko.tm.command.AbstractCommand;

public final class HelpCommand extends AbstractCommand {
    @Override
    public String command() {
        return "help";
    }

    @Override
    public String description() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        if (serviceLocator.getCommands().size() <= 1)
            System.out.println("Command list is empty");
        else {
            for (final AbstractCommand command :
                    serviceLocator.getCommands()) {
                System.out.println(command.command() + ": "
                        + command.description());
            }
        }
    }
}
