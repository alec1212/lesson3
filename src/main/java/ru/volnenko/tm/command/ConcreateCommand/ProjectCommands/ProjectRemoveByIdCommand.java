package ru.volnenko.tm.command.ConcreateCommand.ProjectCommands;

import ru.volnenko.tm.command.AbstractCommand;
import ru.volnenko.tm.entity.Project;

public final class ProjectRemoveByIdCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-remove-by-id";
    }

    @Override
    public String description() {
        return "Remove project by id.";
    }

    @Override
    public void execute() {

        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long id = serviceLocator.getTerminalService().nextLong();
        final Project project = serviceLocator.getProjectService().removeById(id);
        if (project == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");

    }
}
