package ru.volnenko.tm.command.ConcreateCommand.StorageCommands;

import ru.volnenko.tm.command.AbstractCommand;

public final class SaveToJsonCommand extends AbstractCommand {
    @Override
    public String command() {
        return "save-to-json";
    }

    @Override
    public String description() {
        return "Save data to json file.";
    }

    @Override
    public void execute() {

        serviceLocator.getStorageService().saveToJson();

        System.out.println("Выполнено сохранение в файл data/jsonStorage.dat");


    }
}
