package ru.volnenko.tm.command.ConcreateCommand.ProjectCommands;

import ru.volnenko.tm.command.AbstractCommand;

public final class ProjectClearCommand extends AbstractCommand {
    @Override
    public String command() {
        return "projects-clear";
    }

    @Override
    public String description() {
        return "Clear the projects.";
    }

    @Override
    public void execute() {

        System.out.println("[CLEAR PROJECT]");
        serviceLocator.getProjectService().clear();
        System.out.println("[OK]");


    }
}
