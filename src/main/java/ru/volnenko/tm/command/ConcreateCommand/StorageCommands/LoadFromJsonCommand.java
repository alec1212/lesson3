package ru.volnenko.tm.command.ConcreateCommand.StorageCommands;

import ru.volnenko.tm.command.AbstractCommand;

public final class LoadFromJsonCommand extends AbstractCommand {
    @Override
    public String command() {
        return "load-from-json";
    }

    @Override
    public String description() {
        return "Load data from json file.";
    }

    @Override
    public void execute() {

        serviceLocator.getStorageService().loadFromJson();

        System.out.println("Выполнена загрузка данных из  файла data/jsonStorage.dat");


    }
}
