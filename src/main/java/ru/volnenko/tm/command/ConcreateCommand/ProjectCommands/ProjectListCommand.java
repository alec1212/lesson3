package ru.volnenko.tm.command.ConcreateCommand.ProjectCommands;

import ru.volnenko.tm.command.AbstractCommand;
import ru.volnenko.tm.entity.Project;

public final class ProjectListCommand extends AbstractCommand {
    @Override
    public String command() {
        return "projects-list";
    }

    @Override
    public String description() {
        return "List projects.";
    }

    @Override
    public void execute() {

        System.out.println("[LIST PROJECT]");
        int index = 1;
        for (final Project project: serviceLocator.getProjectService().findAll()) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
        System.out.println("[OK]");

    }
}
