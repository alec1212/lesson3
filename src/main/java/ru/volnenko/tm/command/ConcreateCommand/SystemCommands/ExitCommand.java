package ru.volnenko.tm.command.ConcreateCommand.SystemCommands;

import ru.volnenko.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {
    @Override
    public String command() {
        return "exit";
    }

    @Override
    public String description() {
        return "Exit application";
    }

    @Override
    public void execute() {

        System.exit(0);
        }
    }

