package ru.volnenko.tm.command.ConcreateCommand.ProjectCommands;

import ru.volnenko.tm.command.AbstractCommand;
import ru.volnenko.tm.entity.Project;

public final class ProjectRemoveByIndexCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-remove-by-index";
    }

    @Override
    public String description() {
        return "Remove project by index.";
    }

    @Override
    public void execute() {

        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("PLEASE, ENTER PROJECT INDEX:");
        final int id = serviceLocator.getTerminalService().nextInt() - 1;
        final Project project = serviceLocator.getProjectService().removeByIndex(id);
        if (project == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");

    }
}
