package ru.volnenko.tm.command.ConcreateCommand.TaskCommands;

import ru.volnenko.tm.command.AbstractCommand;
import ru.volnenko.tm.entity.Project;
import ru.volnenko.tm.entity.Task;

public final class TaskRemoveByNameCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-remove-by-name";
    }

    @Override
    public String description() {
        return "Remove task by name.";
    }

    @Override
    public void execute() {

        System.out.println("[REMOVE TASK BY NAME]");
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String name = serviceLocator.getTerminalService().nextLine();
        final Task task = serviceLocator.getTaskService().removeByName(name);
        if (task == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");

    }
}
