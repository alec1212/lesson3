package ru.volnenko.tm.command.ConcreateCommand.TaskCommands;

import ru.volnenko.tm.command.AbstractCommand;

public final class TaskClearCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Clear tasks.";
    }

    @Override
    public void execute() {

        System.out.println("[CLEAR TASK]");
        serviceLocator.getTaskService().clear();
        System.out.println("[OK]");


    }
}
