package ru.volnenko.tm.command.ConcreateCommand.StorageCommands;

import ru.volnenko.tm.command.AbstractCommand;

public final class LoadFromXmlCommand extends AbstractCommand {
    @Override
    public String command() {
        return "load-from-xml";
    }

    @Override
    public String description() {
        return "Load data from xml file.";
    }

    @Override
    public void execute() {

        serviceLocator.getStorageService().loadFromXml();

        System.out.println("Выполнена загрузка данных из  файла data/xmlStorage.dat");


    }
}
