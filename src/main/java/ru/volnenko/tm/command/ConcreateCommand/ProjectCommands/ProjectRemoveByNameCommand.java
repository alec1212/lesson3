package ru.volnenko.tm.command.ConcreateCommand.ProjectCommands;

import ru.volnenko.tm.command.AbstractCommand;
import ru.volnenko.tm.entity.Project;

public final class ProjectRemoveByNameCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-remove-by-name";
    }

    @Override
    public String description() {
        return "Remove project by name.";
    }

    @Override
    public void execute() {

        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = serviceLocator.getTerminalService().nextLine();
        final Project project = serviceLocator.getProjectService().removeByName(name);
        if (project == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");

    }
}
