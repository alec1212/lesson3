package ru.volnenko.tm.command.ConcreateCommand.TaskCommands;

import ru.volnenko.tm.command.AbstractCommand;
import ru.volnenko.tm.entity.Task;

public final class TaskListCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-list";
    }

    @Override
    public String description() {
        return "List tasks.";
    }

    @Override
    public void execute() {

        System.out.println("[LIST TASK]");
        int index = 1;
        for (final Task task: serviceLocator.getTaskService().findAll()) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
        System.out.println("[OK]");

    }
}
