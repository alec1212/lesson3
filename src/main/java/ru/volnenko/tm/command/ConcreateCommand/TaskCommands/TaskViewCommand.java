package ru.volnenko.tm.command.ConcreateCommand.TaskCommands;

import ru.volnenko.tm.command.AbstractCommand;
import ru.volnenko.tm.entity.Task;

public final class TaskViewCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-view";
    }

    @Override
    public String description() {
        return "View task.";
    }

    @Override
    public void execute() {

        System.out.println("ENTER, TASK INDEX:");
        final int index = serviceLocator.getTerminalService().nextInt() - 1;
        final Task task = serviceLocator.getTaskService().findByIndex(index);
        if (task == null)
            return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: "+ task.getId());
        System.out.println("NAME: "+ task.getName());
        System.out.println("DESCRIPTION: "+ task.getDescription());
        System.out.println("[OK]");

    }
}
