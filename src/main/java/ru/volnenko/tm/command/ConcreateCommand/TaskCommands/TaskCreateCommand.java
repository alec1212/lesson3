package ru.volnenko.tm.command.ConcreateCommand.TaskCommands;

import ru.volnenko.tm.command.AbstractCommand;

public final class TaskCreateCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String description() {
        return "Create a new Task.";
    }

    @Override
    public void execute() {

        System.out.println("[CREATE TASK]");
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String name = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getTaskService().create(name);
        System.out.println("[OK]");


    }
}
