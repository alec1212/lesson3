package ru.volnenko.tm.command.ConcreateCommand.SystemCommands;

import ru.volnenko.tm.command.AbstractCommand;

public final class VersionCommand extends AbstractCommand {
    @Override
    public String command() {
        return "version";
    }

    @Override
    public String description() {
        return "Show application version.";
    }

    @Override
    public void execute() {
        System.out.println("Task manager. Version 1.0.0");
        }
    }

