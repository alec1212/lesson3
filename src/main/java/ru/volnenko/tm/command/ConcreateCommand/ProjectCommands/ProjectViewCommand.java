package ru.volnenko.tm.command.ConcreateCommand.ProjectCommands;

import ru.volnenko.tm.command.AbstractCommand;
import ru.volnenko.tm.entity.Project;

public final class ProjectViewCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-view";
    }

    @Override
    public String description() {
        return "View project.";
    }

    @Override
    public void execute() {

        System.out.println("ENTER, PROJECT INDEX:");
        final int index = serviceLocator.getTerminalService().nextInt() - 1;
        final Project project = serviceLocator.getProjectService().findByIndex(index);
        if (project == null) return;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: "+ project.getId());
        System.out.println("NAME: "+ project.getName());
        System.out.println("DESCRIPTION: "+ project.getDescription());
        System.out.println("[OK]");

    }
}
