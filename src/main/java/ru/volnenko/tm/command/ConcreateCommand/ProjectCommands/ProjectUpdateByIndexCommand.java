package ru.volnenko.tm.command.ConcreateCommand.ProjectCommands;

import ru.volnenko.tm.command.AbstractCommand;
import ru.volnenko.tm.entity.Project;

public final class ProjectUpdateByIndexCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-update-by-index";
    }

    @Override
    public String description() {
        return "Update project by index.";
    }

    @Override
    public void execute() {

        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER, PROJECT INDEX:");
        final int index = Integer.parseInt(serviceLocator.getTerminalService().nextLine()) -1;
        final Project project = serviceLocator.getProjectService().findByIndex(index);
        if (project == null) {
            System.out.println("FAIL]");
        }
        else {
            System.out.println("PLEASE, ENTER PROJECT NAME:");
            final String name = serviceLocator.getTerminalService().nextLine();
            System.out.println("PLEASE, ENTER PROJECT DESCRIPTION:");
            final String description = serviceLocator.getTerminalService().nextLine();
            serviceLocator.getProjectService().update(project.getId(), name, description);
            System.out.println("[OK]");
        }

    }
}
