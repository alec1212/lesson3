package ru.volnenko.tm.command.ConcreateCommand.StorageCommands;

import ru.volnenko.tm.command.AbstractCommand;

public final class SaveToXmlCommand extends AbstractCommand {
    @Override
    public String command() {
        return "save-to-xml";
    }

    @Override
    public String description() {
        return "Save data to xml file.";
    }

    @Override
    public void execute() {

        serviceLocator.getStorageService().saveToXml();

        System.out.println("Выполнено сохранение в файл data/xmlStorage.dat");


    }
}
