package ru.volnenko.tm.command.ConcreateCommand.TaskCommands;

import ru.volnenko.tm.command.AbstractCommand;
import ru.volnenko.tm.entity.Project;
import ru.volnenko.tm.entity.Task;

public final class TaskRemoveByIdCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-remove-by-id";
    }

    @Override
    public String description() {
        return "Remove task by id.";
    }

    @Override
    public void execute() {

        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("PLEASE, ENTER TASK ID:");
        final long id = serviceLocator.getTerminalService().nextLong();
        final Task task = serviceLocator.getTaskService().removeById(id);
        if (task == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");

    }
}
