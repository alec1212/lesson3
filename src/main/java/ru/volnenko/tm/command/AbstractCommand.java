package ru.volnenko.tm.command;

import ru.volnenko.tm.service.Bootstrap;

public abstract class AbstractCommand {
    protected Bootstrap serviceLocator;
    public void setBootstrap(Bootstrap serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
    public abstract String command();
    public abstract String description();
    public abstract void execute() throws Exception;
}