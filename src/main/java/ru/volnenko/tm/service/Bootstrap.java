package ru.volnenko.tm.service;

import ru.volnenko.tm.command.AbstractCommand;
import ru.volnenko.tm.repository.ProjectRepository;
import ru.volnenko.tm.repository.TaskRepository;

import java.util.*;

public final class Bootstrap {

    private final Scanner terminalService = new Scanner(System.in);

    private final TaskRepository taskRepository = new TaskRepository();

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskService taskService = new TaskService(taskRepository);

    private final StorageService storageService = new StorageService(projectRepository, taskRepository );

    public ProjectService getProjectService() { return projectService;}

    public TaskService getTaskService() { return taskService;}

    public StorageService getStorageService() { return storageService;}

    public Scanner getTerminalService() {return terminalService;}

    {
        taskRepository.create("TASK 1");
        taskRepository.create("TASK 2");
        projectRepository.create("PROJECT 1");
        projectRepository.create("PROJECT 2");
    }

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    public void registry(final AbstractCommand command) {
        final String cliCommand = command.command();
        final String cliDescription = command.description();
        if (cliCommand == null || cliCommand.isEmpty()) try {
            throw new Exception();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (cliDescription == null || cliDescription.isEmpty()) try {
            throw new Exception();
        } catch (Exception e) {
            e.printStackTrace();
        }
        command.setBootstrap(this);
        commands.put(cliCommand, command);
    }
    public void start() throws Exception {
        final Scanner terminalService = new Scanner(System.in);
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            command = terminalService.nextLine();
            execute(command);
        }
    }
    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) {
            System.out.println("Unknown command");
            return;
        }
        abstractCommand.execute();
    }
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }
}